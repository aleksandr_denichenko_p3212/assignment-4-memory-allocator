#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <sys/mman.h>

struct test_details
{
    void *heap;
    size_t size_h;
    uint8_t *malloc[8];
    size_t length;
    size_t size;
};

static void de_bug_message(const void *const heap, const char *const test, const char *const message)
{
    printf("\"%s\": %s\n", test, message);
    debug_heap(stdout, heap);
}

// static void free_mem(struct test_details *const data)
// {
//     for (size_t i = 0; i < data->length; ++i)
//         _free(data->malloc[i]);
//     munmap(data->heap, size_from_capacity((block_capacity){.bytes = data->size_h}).bytes);
// }

static _Noreturn void test_fail(const char *const test, const char *const message)
{
    heap_term();
    err("\"%s\": %s\n", test, message);
}

static void alloc_memory(struct test_details *const data, const char *const test)
{
    for (size_t i = 0; i < data->length; ++i)
    {
        if (data->malloc[i])
            continue;

        data->malloc[i] = _malloc(data->size);

        if (!data->malloc[i])
            test_fail(test, "isn't passed");
    }
}

static void run_test(struct test_details *const data, const char *const test)
{
    printf("\"%s\"", test);
    data->heap = heap_init(data->size_h);
    if (!data->heap)
        err("\"%s\": isn't init heap", test);
    de_bug_message(data->heap, test, "init heap");
    alloc_memory(data, test);
}

static void stop_test(struct test_details *const data, const char *const test)
{
    de_bug_message(data->heap, test, "end");
    heap_term();
    printf("\"%s\": %s\n\n", test, "passed");
}

void mem_allocate()
{
    const char *test = "malloc";

    struct test_details data = {
        .size_h = REGION_MIN_SIZE,
        .length = 1,
        .size = REGION_MIN_SIZE / 2};

    run_test(&data, test);

    de_bug_message(data.heap, test, "Memory allocated");

    stop_test(&data, test);
}

void free_one()
{
    const char *test = "Free one block in some blocks";

    struct test_details data = {
        .size_h = REGION_MIN_SIZE,
        .length = 3,
        .size = REGION_MIN_SIZE / 4};
    run_test(&data, test);

    de_bug_message(data.heap, test, "Memory is allocated");

    _free(data.malloc[1]);

    if (!data.malloc[0] || !data.malloc[2])
        test_fail(test, "Memory damaged");
    de_bug_message(data.heap, test, " after freeing second block");
    stop_test(&data, test);
}

void free_two()
{
    const char *test = "Free two blocks in some blocks";

    struct test_details data = {
        .size_h = REGION_MIN_SIZE,
        .length = 4,
        .size = REGION_MIN_SIZE / 5};
    run_test(&data, test);

    de_bug_message(data.heap, test, "Memory is allocated");

    _free(data.malloc[1]);
    _free(data.malloc[2]);

    if (!data.malloc[0] || !data.malloc[3])
        test_fail(test, "Memory damaged");
    de_bug_message(data.heap, test, " after freeing second and third block");

    stop_test(&data, test);
}

void allocate_huge_mem()
{
    const char *test = "Allocate huge memory";

    struct test_details data = {
        .size_h = REGION_MIN_SIZE,
        .length = 1,
        .size = REGION_MIN_SIZE * 2};
    run_test(&data, test);

    de_bug_message(data.heap, test, "Memory is allocated");

    const struct block_header *header = (struct block_header *)data.heap;
    data.size_h = size_from_capacity(header->capacity).bytes;

    if (data.size_h < 2 * REGION_MIN_SIZE)
        test_fail(test, "Heap isn't extended");

    stop_test(&data, test);
}

void allocate_another_place()
{
    const char *test = "Allocate memory in another place";

    struct test_details data = {
        .size_h = REGION_MIN_SIZE,
        .length = 1,
        .size = REGION_MIN_SIZE};
    run_test(&data, test);

    de_bug_message(data.heap, test, "First block is allocated");

    const struct block_header *header = (struct block_header *)(data.malloc[0] - offsetof(struct block_header, contents));

    void *alloc_region = mmap(
        (void *)(header->contents + header->capacity.bytes),
        REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    data.length = 2;
    alloc_memory(&data, test);

    de_bug_message(data.heap, test, "Second block is allocated");

    data.size_h = size_from_capacity(((struct block_header *)data.heap)->capacity).bytes;
    const size_t region_size = size_from_capacity((block_capacity){REGION_MIN_SIZE}).bytes;

    stop_test(&data, test);
    munmap(alloc_region, region_size);
}

int main(void)
{
    mem_allocate();
    free_one();
    free_two();
    allocate_huge_mem();
    allocate_another_place();
    return 0;
}